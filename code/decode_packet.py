import struct
import socket

TCP_TYPE = 6

def decode_packet(pktlen, data, timestamp):
    if not data:
        return
    if data[12:14] == '\x08\x00':
        packet = data[14:]
        ip_header = packet[0:20]

        iph = struct.unpack('!BBHHHBBH4s4s', ip_header)
        version_ihl = iph[0]
        # version = version_ihl >> 4
        ihl = version_ihl & 0xF
        iph_length = ihl * 4
        # ttl = iph[5]
        protocol = iph[6]
        if protocol != TCP_TYPE 
        s_addr = socket.inet_ntoa(iph[8])
        d_addr = socket.inet_ntoa(iph[9])

        tcp_header = packet[iph_length:iph_length+20]


        tcph = struct.unpack('!HHLLBBHHH', tcp_header)
        source_port = tcph[0]
        dest_port = tcph[1]
        sequence = tcph[2]
        # acknowledgement = tcph[3]
        doff_reserved = tcph[4]
        flag = tcph[5]
        tcph_length = doff_reserved >> 4

        h_size = iph_length + tcph_length * 4
        # data_size = len(packet) - h_size

        # get data from the packet
        data = packet[h_size:]
        assembler(
            data,
            s_addr,
            d_addr,
            source_port,
            dest_port,
            flag,
            str(sequence)
            )
