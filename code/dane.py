@handler(handlers, isHandler=config.V_DANE)
class Dane(BaseHandler):

    name = "dane"

    def __init__(self, cert, ocsp):
        super(Dane, self).__init__()
        self.on_certificate(cert)

    def verify_chain(self, cert):
        """ Same functionality as rfcnss"""
        pass

    def on_certificate(self, cert):

        def verify(url):
            try:
                answer = dns.resolver.query('_443._tcp.' + url, 'TLSA')
            except:
                return False

            (
                cert_usage,
                selector,
                match_type,
                associated_data
            ) = [str(ans) for ans in answer][0].split(' ')
            funct = [cert.der_data, cert.subject_public_key_info]
            hash_funct = [None, hashlib.sha256, hashlib.sha512]
            temp = hash_funct[int(match_type)]


            if cert_usage == '3' or cert_usage == '1':
                data = funct[int(selector)]()
                if temp is not None:
                    m = temp(data)
                    data = m.hexdigest()
                if data == associated_data:
                    return True
                else:
                    return False

            if cert_usage == '0' or cert_usage == '2':
                # We must check for each certificate in the chain that the
                # associated data is presented

                for cer in xrange(0, cert.length_chain()):
                    data = funct[int(selector)](deep=cer)
                    if temp is not None:
                        m = temp(data)
                        data = m.hexdigest()

                    if data == associated_data:
                        if cert_usage == '0':
                            return True
                        else:
                            cert.add_to_nssdb(
                                cert.subject_common_name(deep=cer),
                                deep=cer)
                            value = self.verify_chain(cert)
                            cert.remove_from_nssdb(
                                cert.subject_common_name(deep=cer)
                                )
                            return value
                return False

        try:
            url = cert.subject_common_name()
        except IndexError:
            debug_logger.debug("\t[-] ERROR extracting subject_common_name")
            return

        result = False
        result = verify(url)

        if result is True:
            debug_logger.debug(
                "\t[+] Certificate %s has a valid TLSA record" %
                cert.ca_name()
                )
            return

        if url[0:3] == "www":
            url = url.replace("www.", '')
            result = verify(url)

        if url[0] == '*':
            url = url.replace('*', 'www')
            result = verify(url)
            if result is True:
                debug_logger.debug(
                    "\t[+] Certificate %s has a valid TLSA record" %
                    cert.ca_name()
                    )
                return

            url = url.replace('www', '*')

        if url[0] == '*.':
            url = url.replace('*.', '')
            result = verify(url)

        if result is True:
            debug_logger.debug(
                "\t[+] Certificate %s has a valid TLSA record" %
                cert.ca_name()
                )
            return
        debug_logger.debug(
            "\t[-] Certificate {0} has not a valid TLSA".format(
                cert.ca_name()) + " record or it doesn't implement DANE")
