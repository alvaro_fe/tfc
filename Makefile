#!/usr/bin/make -f
PROJ=main
PDFOPT=-synctex=1 -file-line-error -interaction nonstopmode

all:	$(PROJ).pdf

# The rest of the files
SRC = main.tex portada.tex previo.tex introduction.tex acronyms.tex \
	  tlspki.tex include.tex tls.tex pki.tex solutions.tex \
	  implementation.tex solutions/chainoftrust.tex \
	  solutions/dane.tex solutions/pinning.tex solutions/black-icsi.tex \
		solutions/ct.tex

# Images, figures included in document
#IMGS =	figures/deployment.pdf		figures/osgi-cycle.pdf \


# Bibliography
BIB =	rfc.bib
#biblio.bib rfc.bib biblio-3.bib biblio-netide.bib

$(PROJ).bbl:	$(PROJ).tex $(SRC) $(BIB)
	pdflatex $(PDFOPT) $(PROJ)
	bibtex $(PROJ)

$(PROJ).pdf:	$(PROJ).bbl $(IMGS)
	- [ -f figures/Makefile ] && $(MAKE) -C figures
	pdflatex $(PDFOPT) $(PROJ)	
	pdflatex $(PDFOPT) $(PROJ)
	open main.pdf

clean:
	- [ -f figures/Makefile ] && $(MAKE) -C figures $@
	rm -vf *~ \
		$(PROJ).bbl $(PROJ).blg $(PROJ).aux $(PROJ).lo[fgtx] $(PROJ).out\
		$(PROJ).toc $(PROJ).synctex.gz $(PROJ).up[ab] $(PROJ).f*


realclean:	clean
	$(MAKE) -C figures $@
	rm -vf $(PROJ).pdf

rebuild: realclean all

